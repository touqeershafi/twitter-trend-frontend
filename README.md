# TwitterTrendFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Environment Configuration

Check `./src/environments/environment.ts` for API Endpoint & other configuration settings

## Install Dependencies using npm

`npm install`