import {Component, OnInit, OnDestroy} from '@angular/core';
import {TwitterService} from "./shared/service/twitter.service";
import {Observable, of, timer, Subscription} from "rxjs";
import {Trend} from "./shared/model/trend";
import {timeInterval, pluck, take} from "rxjs/operators";
import {environment} from "../environments/environment";
import {BsModalService, ModalOptions} from "ngx-bootstrap";
import {AddLocationComponent} from "./shared/popup/add-location/add-location.component";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

    public trends: Observable<Trend[]> = of([]);
    public currentLocation;
    public localTrendLocationStorageKey = `${environment.trend_current_location_storage_key}`;
    public timer: Subscription;
    public isLoading = false;

    public defaultLocations = [
        { country_name: "United States", country_woeid: "23424977" },
        { country_name: "England", country_woeid: "23424975" },
        { country_name: "Germany", country_woeid: "638242" },
    ];
    public savedLocations = [];

    ngOnInit() {

        if (localStorage[this.localTrendLocationStorageKey]) {
            this.currentLocation = localStorage[this.localTrendLocationStorageKey];
            this.trends = of(JSON.parse(localStorage[`${environment.trend_location_storage_key}_${this.currentLocation}`] || '[]'));
            this.startTimer(1000 * 20);
        }


        this.savedLocations = JSON.parse(localStorage['country_list'] || '[]')
    }

    constructor(private twitterService: TwitterService, private modalService: BsModalService) {

    }

    startTimer(startTime = 0) {
        if (this.timer) {
            this.timer.unsubscribe();
        }

        this.timer = timer(startTime, 1000 * 20).subscribe((next) => {
            this.getTrends(next == 0);
        });

    }

    getTrends(showLoader = false) {
        this.isLoading = showLoader;
        this.twitterService.getTrends(this.currentLocation).subscribe((response: any) => {
            if (response.status) {
                localStorage[this.localTrendLocationStorageKey] = this.currentLocation;
                this.trends = of(response.data.trends);
                localStorage[`${environment.trend_location_storage_key}_${this.currentLocation}`] = JSON.stringify(response.data.trends);
            } else {
                this.timer.unsubscribe();
                alert(response.error);
            }
            this.isLoading = false;
        });
    }

    openAddLocationModal() {
        let modelRef = this.modalService.show(AddLocationComponent, { initialState: { defaultLocations: this.defaultLocations } });
        this.modalService.onHide.subscribe(() => {
            this.savedLocations = JSON.parse(localStorage['country_list'] || '[]')
        })
    }

    ngOnDestroy() {

    }

}
