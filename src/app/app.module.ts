import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {AppComponent} from './app.component';
import {FooterComponent} from './shared/component/footer/footer.component';
import {HeaderComponent} from './shared/component/header/header.component';
import {TwitterService} from "./shared/service/twitter.service";
import {ModalModule} from "ngx-bootstrap";
import { AddLocationComponent } from './shared/popup/add-location/add-location.component';

@NgModule({
    declarations: [
        AppComponent,
        FooterComponent,
        HeaderComponent,
        AddLocationComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule.forRoot()
    ],
    entryComponents: [AddLocationComponent],
    providers: [TwitterService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
