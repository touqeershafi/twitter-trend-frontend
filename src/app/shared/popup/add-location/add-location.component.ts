import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {Validators, FormControl, FormGroup, FormBuilder} from "@angular/forms";

@Component({
    selector: 'app-add-location',
    templateUrl: './add-location.component.html',
    styleUrls: ['./add-location.component.css']
})
export class AddLocationComponent implements OnInit {

    public country_name;
    public country_woeid;
    public defaultLocations;

    public countryForm: FormGroup;

    constructor(public bsModalRef: BsModalRef, private formBuilder: FormBuilder) {


    }


    ngOnInit() {

        this.countryForm = this.formBuilder.group({
            'country_name': ['', Validators.required],
            'country_woeid': ['', Validators.required]
        });


    }

    onSubmit($form: any) {
        if (this.countryForm.valid) {

            let countryList = JSON.parse(localStorage['country_list'] || '[]');

            let hasCountryInTheSavedList = countryList.filter(($item) => {
                return $item.country_woeid == this.countryForm.value.country_woeid ||
                    $item.country_name == this.countryForm.value.country_name;
            });

            let hasCountryInTheDefaultList = this.defaultLocations.filter(($item) => {
                return $item.country_woeid == this.countryForm.value.country_woeid ||
                    $item.country_name == this.countryForm.value.country_name;
            });

            if (hasCountryInTheSavedList.length > 0 || hasCountryInTheDefaultList.length > 0) {
                alert("Location is already inserted!");
                return;
            }

            countryList.push({
                country_name: this.countryForm.value.country_name,
                country_woeid: this.countryForm.value.country_woeid
            });

            localStorage['country_list'] = JSON.stringify(countryList);
            return this.bsModalRef.hide();
        }

        this.markFormGroupTouched(this.countryForm);
    }

    private markFormGroupTouched(formGroup: FormGroup) {
        (<any>Object).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
            control.markAsDirty();
        });
    }

}
