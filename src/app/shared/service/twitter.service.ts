import { Injectable } from '@angular/core';
import {Trend} from "../model/trend";
import {Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  trends;

  constructor(private http: HttpClient) { }

  getTrends(location: string) : Observable<any> {
    return this.http.get<Trend[]>(`${environment.api_end_point}/trends`, { params: { id: location } });
  }

}
