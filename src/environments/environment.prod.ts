export const environment = {
  production: true,
  api_end_point: "http://localhost/twitter-trend/public", // Don't Add Last Slash
  trend_location_storage_key: "trends",
  trend_current_location_storage_key: "current_trend"
};
